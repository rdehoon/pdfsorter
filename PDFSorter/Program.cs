﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PDFSorter
{
    internal class Program
    {

        static void Main(string[] args)
        {
            // Handel de CommandLine Arguments af
            uint result = HandleArguments(args);
            switch (result)
            {
                case 0:
                    geenArgumenten();
                    break;
                case 1:
                    PrintManual();
                    break;
                case 2:
                    MigratePDF(args[0], false, 6);
                    break;
                case 3:
                    MigratePDF(args[0], true, 6);
                    break;
                case 4:
                    MigratePDF(args[0], false, 6, args[1]);
                    break;
                case 5:
                    MigratePDF(args[0], false, int.Parse(args[1]));
                    break;
                case 6:
                    MigratePDF(args[0], true, int.Parse(args[2]));
                    break;
                case 7:
                    MigratePDF(args[0], true, 6, args[2]);
                    break;
                case 8:
                    MigratePDF(args[0], false, int.Parse(args[1]), args[2]);
                    break;
                case 9:
                    MigratePDF(args[0], true, int.Parse(args[2]), args[3]);
                    break;

                default:
                    geenArgumenten();
                    break;
            }
        }

        private static void geenArgumenten()
        {
            Console.WriteLine("Geen argumenten gegeven.\n\r" +
                "Typ pdfsorter ? voor de handleiding.");
        }

        private static uint HandleArguments(string[] arguments)
        {
            int numberOfArguments = arguments.Length;
            /*
             * De volledige syntax van PDFSorter is: PDFSorter pad [-v] [aantal] [extensie]
             * Er kan ook om hulp gevraagd worden door PDFSorter ? te doen.
             * 
             * De mogelijke combinaties zijn dus (met bijbehorende statemodus):
             * 
             * Bij geen argumenten:
             * PDFSorter                                            0
             * 
             * Bij één argument:
             * PDFSorter    ?                                       1
             * PDFSorter    pad                                     2
             * 
             * Bij twee argumenten:
             * PDFSorter    pad -v                                  3
             * PDFSorter    pad [aantal]                            4
             * PDFSorter    pad [extensie]                          5
             * 
             * Bij drie argumenten:
             * PDFSorter    pad -v          [aantal]                6
             * PDFSorter    pad -v          [extensie]              7
             * PDFSorter    pad [aantal]    [extensie]              8
             * 
             * Bij vier argumenten:
             * PDFSorter    pad -v          [aantal]    [extensie]  9
             * 
             * TODO: Misschien dat met RegExp een meer handige oplossing mogelijk is...
             */

            // PDFSorter                                            0

            if (numberOfArguments == 0)
            {
                // niks opgegeven
                return 0;
            }

            /*
             * PDFSorter    ?                                       1
             * PDFSorter    pad                                     2
             */
            if (numberOfArguments == 1)
            {
                if (arguments[0] == "?")
                {
                    return 1;
                }
                else
                {
                    return 2; //De verificatie van het pad vindt plaats in het model MigratePDF
                }

            }

            /*
             * PDFSorter    pad -v                                  3
             * PDFSorter    pad [aantal]                            4
             * PDFSorter    pad [extensie]                          5   
             */
            if (numberOfArguments == 2)
            {
                int i = 0;
                if (arguments[1] == "-v")
                {
                    return 3;
                }

                // Kijk eerst of het niet een bestandsextensie is
                if (arguments[1].StartsWith("."))
                {
                    return 5;
                }

                if (int.TryParse(arguments[1], out i))
                {
                    return 4;
                }
            }

            /*
             * PDFSorter    pad -v          [aantal]                6
             * PDFSorter    pad -v          [extensie]              7
             * PDFSorter    pad [aantal]    [extensie]              8
             */
            if (numberOfArguments == 3)
            {
                if (arguments[1] == "-v")
                {
                    int i = 0;
                    // Kijk eerst of het niet een bestandsextensie is
                    if (arguments[2].StartsWith("."))
                    {
                        return 7;
                    }

                    if (int.TryParse(arguments[2], out i))
                    {
                        return 6;
                    }
                }
                return 8;
            }

            /*
             * PDFSorter    pad -v          [aantal]    [extensie]  9
             */
            if (numberOfArguments == 4)
            {
                // alle parameters opgegeven
                return 9;
            }

            return 0;
        }

        private static void PrintManual()
        {
            Console.WriteLine("NAAM\n" +
                                "PDFSorter - Vergaart 6 pdf bestanden en\n" +
                                "verplaatst ze naar hun eigen directory.\n\n" +
                                "SAMENVATTING\n" +
                                "PDFSorter [DIR] [OPTIES] [AANTAL] [EXTENSIE]\n\n" +
                                "BESCHRIJVING\n" +
                                "Vergaart 6 pdf bestanden, genaamd \"bestand[N].pdf\" (N==1...6) en\n" +
                                "verplaatst ze naar hun eigen directory.\n" +
                                "De [DIR] is het absolute pad naar directory waar de pdf bestanden verblijven. Deze is, ondermeer, " +
                                "te verkrijgen door te rechtsklikken op de adresbalk in Windows Verkenner en dan te kiezen " +
                                "voor \"Adres als tekst kopiëren.\"\n" +
                                "Met [AANTAL] kan het aantal te verplaatsen bestanden worden aangegeven. " +
                                "Dit parameter is optioneel.\n" +
                                "Met [EXTENSIE] kunnen ook andere bestanden dan pdf verplaatst worden. " +
                                "Ook dit parameter is optioneel.\n" +
                                "OPTIES:\n" +
                                "?\n" +
                                "Geeft dit hulpmenu weer. Kan niet gebruikt worden met een [DIR] opgegeven.\n\n" +
                                "-v\n" +
                                "Staat voor de optie\"Verbose\". Geeft informatie weer tijdens tijdens het verplaatsen.\n" +
                                "Het niet gebruiken van deze optie houdt eventuele foutmeldingen NIET tegen.\n\n" +
                                "Door een [AANTAL] op te geven kan het aantal te verplaatsen bestanden worden aangepast. " +
                                "Standaard worden 6 bestanden verplaatst. Maar hier kan ook een ander aantal worden opgegeven.\n" +
                                "Als hier 0 wordt ingebracht, worden ALLE bestanden gemigreerd.\n\n" +
                                "Met [EXTENSIE] kunnen ook andere type bestanden worden verplaatst. Zorg er wel voor " +
                                "dat de extensie met een punt begint.\n\n" +
                                "AUTEUR\n" +
                                "Rob de Hoon.\n\n" +
                                "FOUTEN RAPPORTEREN\n" +
                                "Fouten kunnen gemeld worden in de GitLab-repo.\n\n" +
                                "COPYRIGHT\n" +
                                "Dit programma valt onder de \"GNU General Public License v3.0\".\n");
        }

        private static void MigratePDF(string pad, bool verbose, int aantal = 0, string extension = ".pdf")
        {
            // Bekijk eerst of het pad bestaat. Zo niet, dan hebben we aan abrupte stop.
            if (!Directory.Exists(pad))
            {
                Console.WriteLine("Directory bestaat niet");
                return;
            }

            // Vergeef het als er een negatief aantal wordt opgegeven.
            aantal = Math.Abs(aantal);

            IEnumerable<string> bestanden = Directory.EnumerateFileSystemEntries(pad, "*" + extension);
            int aantalBestanden = bestanden.Count();

            // Als er geen bestanden zijn, hoeft er ook niets overgedragen te worden...
            if (aantalBestanden == 0)
            {
                Console.WriteLine($"Geen bestanden van het type *{extension} aanwezig.");
                return;
            }

            // Maak de directory om naar te kopieren
            string ptwt = pad + Path.DirectorySeparatorChar
                + extension.Substring(1).ToUpper();
            //ptwt betekent path to write to
            try
            {
                if (!Directory.Exists(ptwt))
                {
                    Directory.CreateDirectory(ptwt);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Er is een fout opgetreden: {0}", e.ToString());
                return;
            }


            /* Bekijk of het aantal over te dragen bestanden, groter of kleiner is dan
            * het aantal aanwezige bestanden.
            */
            int eindPunt = Math.Min(aantalBestanden, aantal);

            //Als er geen aantal is opgegeven, worden alle bestanden overgedragen
            if (aantal == 0)
            {
                eindPunt = aantalBestanden;
            }

            IEnumerator<string> doorloper = bestanden.GetEnumerator();
            int teller = 0;
            while (doorloper.MoveNext() && teller < eindPunt)
            {
                string bestemming = ptwt + Path.DirectorySeparatorChar + doorloper.Current.Substring(pad.Length + 1);
                if (verbose)
                {
                    Console.WriteLine($"Verplaats {doorloper.Current} naar {bestemming}");
                }

                try
                {
                    Directory.Move(doorloper.Current, bestemming);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Er is een fout opgetreden: {0}", e.ToString());
                    return;
                }
                teller++;
            }
        }
    }
}
