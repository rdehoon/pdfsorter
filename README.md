# NAAM
PDFSorter - Vergaart 6 pdf bestanden en verplaatst ze naar hun eigen directory.

# SAMENVATTING
PDFSorter [DIR] [OPTIES] [AANTAL] [EXTENSIE]

# BESCHRIJVING
Vergaart 6 pdf bestanden, genaamd "bestand[N].pdf" (N==1...6) en
verplaatst ze naar hun eigen directory.
De [DIR] is het absolute pad naar directory waar de pdf bestanden verblijven. Deze is, ondermeer, te verkrijgen door te rechtsklikken op de adresbalk in Windows Verkenner en dan te kiezen voor "Adres als tekst kopiëren."
Met [AANTAL] kan het aantal te verplaatsen bestanden worden aangegeven. Dit parameter is optioneel.
Met [EXTENSIE] kunnen ook andere bestanden dan pdf verplaatst worden. Ook dit parameter is optioneel.
# OPTIES:
?
Geeft dit hulpmenu weer. Kan niet gebruikt worden met een [DIR] opgegeven.

-v
Staat voor de optie"Verbose". Geeft informatie weer tijdens tijdens het verplaatsen.
Het niet gebruiken van deze optie houdt eventuele foutmeldingen NIET tegen.

Door een [AANTAL] op te geven kan het aantal te verplaatsen bestanden worden aangepast. Standaard worden 6 bestanden verplaatst. Maar hier kan ook een ander aantal worden opgegeven.
Als hier 0 wordt ingebracht, worden ALLE bestanden gemigreerd.

Met [EXTENSIE] kunnen ook andere type bestanden worden verplaatst. Zorg er wel voor dat de extensie met een punt begint.

# AUTEUR
Rob de Hoon.

# FOUTEN RAPPORTEREN
Fouten kunnen gemeld worden in de GitLab-repo.

# COPYRIGHT
Dit programma valt onder de "GNU General Public License v3.0".
